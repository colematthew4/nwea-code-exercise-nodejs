const AWS = require('aws-sdk');

const dynamodb = new AWS.DynamoDB.DocumentClient({
  apiVersion: '2012-08-10',
  endpoint: new AWS.Endpoint('http://localhost:8000'),
  region: 'us-west-2',
  // what could you do to improve performance?
});

const tableName = 'SchoolStudents';
const studentLastNameGsiName = 'studentLastNameGsi';

/**
 * The entry point into the lambda
 *
 * @param {Object} event
 * @param {string} event.schoolId
 * @param {string} event.studentId
 * @param {string} [event.studentLastName]
 */
exports.handler = (event) => {
  // TODO use the AWS.DynamoDB.DocumentClient to write a query against the 'SchoolStudents' table and return the results.
  // The 'SchoolStudents' table key is composed of schoolId (partition key) and studentId (range key).
  let params = {
    TableName: tableName,
    KeyConditionExpression: '#partKey = :schoolId and #rangeKey = :studentId',
    ExpressionAttributeNames: {
      '#partKey': 'schoolId',
      '#rangeKey': 'studentId'
    },
    ExpressionAttributeValues: {
      ':schoolId': event.schoolId,
      ':studentId': event.studentId
    },
    Limit: 5
  };

  if (!event.studentId) {
    params.KeyConditionExpression = params.KeyConditionExpression.split('and')[0].trim();
    delete params.ExpressionAttributeNames['#rangeKey'];
    delete params.ExpressionAttributeValues[':studentId'];
  }

  // TODO (extra credit) if event.studentLastName exists then query using the 'studentLastNameGsi' GSI and return the results.
  if (event.studentLastName) {
    params = {
      ...params,
      IndexName: studentLastNameGsiName,
      KeyConditionExpression: `#lastNameGsi = :studLastName`,
      ExpressionAttributeNames: {
        '#lastNameGsi': 'studentLastName'
      },
      ExpressionAttributeValues: {
        ':studLastName': event.studentLastName
      }
    }
  }

  // TODO (extra credit) limit the amount of records returned in the query to 5 and then implement the logic to return all
  //  pages of records found by the query (uncomment the test which exercises this functionality)

  return queryNextPage(params);
};

/**
 * Gets all the data from a query, accounting for pagination if requested.
 *
 * @param {AWS.DynamoDB.DocumentClient.QueryInput} queryParams
 * @returns a Promise with all of the data for the given query
 */
async function queryNextPage(queryParams) {
  const data = await dynamodb.query(queryParams).promise();
  if (!data.LastEvaluatedKey) {
    return data.Items;
  }

  queryParams.ExclusiveStartKey = data.LastEvaluatedKey;
  return data.Items.concat(await queryNextPage(queryParams));
}
