const AWS = require('aws-sdk');

const dynamodb = new AWS.DynamoDB.DocumentClient({
  apiVersion: '2012-08-10',
  endpoint: new AWS.Endpoint('http://localhost:8000'),
  region: 'us-west-2',
  // what could you do to improve performance?
});

const tableName = 'SchoolStudents';

/**
 * The entry point into the lambda
 *
 * @param {Object} event
 * @param {string} event.schoolId
 * @param {string} event.schoolName
 * @param {string} event.studentId
 * @param {string} event.studentFirstName
 * @param {string} event.studentLastName
 * @param {string} event.studentGrade
 */
exports.handler = (event) => {
  // TODO validate that all expected attributes are present (assume they are all required)
  const missingProps = validateStudent(event);
  if (missingProps.length) {
    return Promise.reject(`The event is missing the following required fields: ${missingProps}`);
  }

  // TODO use the AWS.DynamoDB.DocumentClient to save the 'SchoolStudent' record
  // The 'SchoolStudents' table key is composed of schoolId (partition key) and studentId (range key).
  const params = {
    TableName: tableName,
    Item: {
      schoolId: event.schoolId,
      studentId: event.studentId,
      schoolName: event.schoolName,
      studentFirstName: event.studentFirstName,
      studentLastName: event.studentLastName,
      studentGrade: event.studentGrade
    }
  };
  return dynamodb.put(params).promise()
    .catch(err => {
      console.error('An error occurred saving the student data', err);
    });
};

/**
 * Validate all the required data is sent to the lambda.
 *
 * @param {Object} event
 * @param {string} event.schoolId
 * @param {string} event.schoolName
 * @param {string} event.studentId
 * @param {string} event.studentFirstName
 * @param {string} event.studentLastName
 * @param {string} event.studentGrade
 * @returns {Array<string>} any missing fields the event must contain.
 */
function validateStudent(event) {
  return [
    'schoolId', 'schoolName',
    'studentId', 'studentFirstName', 'studentLastName', 'studentGrade'
  ].reduce((missingProps, prop) => {
    if (event[prop] == null) {
      missingProps.push(prop);
    }
    return missingProps;
  }, []);
}
